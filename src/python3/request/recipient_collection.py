# -*- coding: utf-8 -*- 
"""
# Copyright (c) Microsoft Corporation.  All Rights Reserved.  Licensed under the MIT License.  See License in the project root for license information.
# 
#  This file was generated and any changes will be overwritten.
"""

from __future__ import unicode_literals

from ..collection_base import CollectionRequestBase, CollectionResponseBase, CollectionPageBase
from ..model.recipient import Recipient
import json
import asyncio


class RecipientsCollectionRequest(CollectionRequestBase):
    def __init__(self, request_url, client, options):
        """Initialize the RecipientsCollectionRequest
        
        Args:
            request_url (str): The url to perform the RecipientCollectionRequest
                on
            client (:class:`GraphClient<msgraph.request.graph_client.GraphClient>`):
                The client which will be used for the request
            options (list of :class:`Option<msgraph.options.Option>`):
                A list of options to pass into the request
        """
        super(RecipientsCollectionRequest, self).__init__(request_url, client, options)

    def get(self):
        """Gets the RecipientsCollectionPage

        Returns: 
            :class:`RecipientsCollectionPage<msgraph.request.recipient_collection.RecipientsCollectionPage>`:
                The RecipientsCollectionPage
        """
        self.method = "GET"
        collection_response = RecipientsCollectionResponse(json.loads(self.send().content))
        return self._page_from_response(collection_response)

    @asyncio.coroutine
    def get_async(self):
        """Gets the RecipientsCollectionPage in async

        Yields: 
            :class:`RecipientsCollectionPage<msgraph.request.recipient_collection.RecipientsCollectionPage>`:
                The RecipientsCollectionPage
        """
        future = self._client._loop.run_in_executor(None,
                                                    self.get)
        collection_page = yield from future
        return collection_page


class RecipientsCollectionResponse(CollectionResponseBase):
    @property
    def collection_page(self):
        """The collection page stored in the response JSON
        
        Returns:
            :class:`RecipientsCollectionPage<msgraph.request.recipient_collection.RecipientsCollectionPage>`:
                The collection page
        """
        if self._collection_page:
            self._collection_page._prop_list = self._prop_dict["value"]
        else:
            self._collection_page = RecipientsCollectionPage(self._prop_dict["value"])

        return self._collection_page


class RecipientsCollectionPage(CollectionPageBase):
    def __getitem__(self, index):
        """Get the Recipient at the index specified
        
        Args:
            index (int): The index of the item to get from the RecipientsCollectionPage

        Returns:
            :class:`Recipient<msgraph.model.recipient.Recipient>`:
                The Recipient at the index
        """
        return Recipient(self._prop_list[index])

    def recipient(self):
        """Get a generator of Recipient within the RecipientsCollectionPage
        
        Yields:
            :class:`Recipient<msgraph.model.recipient.Recipient>`:
                The next Recipient in the collection
        """
        for item in self._prop_list:
            yield Recipient(item)

    def _init_next_page_request(self, next_page_link, client, options):
        """Initialize the next page request for the RecipientCollectionPage
        
        Args:
            next_page_link (str): The URL for the next page request
                to be sent to
            client (:class:`GraphClient<msgraph.model.graph_client.GraphClient>`:
                The client to be used for the request
            options (list of :class:`Option<msgraph.options.Option>`:
                A list of options
        """
        self._next_page_request = RecipientsCollectionRequest(next_page_link, client, options)
